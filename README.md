# USAGE: #

Clone project (it will fetch all submodules automatically)

```bash
git clone git@bitbucket.org:redbonesmith/banderol.git
```

update submodules:

```bash
git submodule update --init
git pull
```

add `--recursive` for recursive :)

foreach git pull: 
```bash
git submodule foreach 'git checkout master && git pull origin master'
```


# workflow:
* For new feature | bug create new branch with such name :

```bash
git checkout -b pt-3_create-feature-or-fix-bug
```

where `pt` is pivotal or `bb` as bitbucket etc, number is issue number on tracker and short description

* After committing push remote branch and create pull request
```bash
git push --set-upstream origin pt-3_create-feature-or-fix-bug
```


### Who do I talk to? ###
* Repo owner or admin: @redbonesmith